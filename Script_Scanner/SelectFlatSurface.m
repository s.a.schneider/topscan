function [Plane,IndexPlane]=SelectFlatSurface(Points,Settings)
%selectflatsurface lets user select a flat surface within a point cloud
%   [Plane,IndexPlane]=selectflatsurface(Points,Settings)
%
%   [Points] : structure containing X,Y and Z coordinates in laser scanner
%   coordinates.
%
%   [Settings] : structure containing settings. Should at least contain:
%       Settings.iStart : index of file that represents first scan
%
%   [Plane] : structure containing X,Y and Z coordinates of plane in laser
%   scanner coordinates.
%
%   [IndexPlane] : indexes of Points that have been selected for [Plane]
%
%
%   DBT Broerse 2016-3-14
%   current version 2016-11-16
%   questions, bugs mail to d.b.t.broerse uu nl
if nargin < 2
    j = 1; 
    Points2{j}=Points;
    clear Points
    Points = Points2;
else
    j = Settings.iStart;
end



figure; hold on
scatter3(Points{j}.X,Points{j}.Y,Points{j}.Z,5,Points{j}.Z);view([0 0 1])
axis image
title('draw rectangle containing only area with flat plane')

rect = getrect(gcf);
% rect contains [xmin ymin width height]


%% extract points within rect
IndexPlane=find(Points{j}.X<=rect(1)+rect(3)&Points{j}.X>=rect(1)&Points{j}.Y<=rect(2)+rect(4)&Points{j}.Y>=rect(2));

Plane.X=Points{j}.X(IndexPlane);
Plane.Y=Points{j}.Y(IndexPlane);
Plane.Z=Points{j}.Z(IndexPlane);

%%
figure;hold on
scatter3(Plane.X,Plane.Y,Plane.Z,2,Plane.Z);colorbar;view([0 0 1])
title('selected plane')


end