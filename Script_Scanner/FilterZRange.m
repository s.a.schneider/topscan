function [ModelPoints,Outliers]=FilterZRange(ModelPoints,Settings)
%SelectModel lets user select points that contain the area of interest
%   [ModelPoints,Outliers]=FilterZRange(ModelPoints,Settings)
%
%   [ModelPoints] : structure containing ected X,Y and Z coordinates in
%   ected coordinates of the model area only.
%
%   [Settings] : structure containing settings. Should at least contain:
%       Settings.ZRange : range of z, values outside this range will be
%       removed
%
%   [ModelPoints,Outliers]=FilterZRange(ModelPoints,Settings)
%
%   [Outliers] : Information on outliers

%   DBT Broerse 2016-3-14
%   current version 2016-11-16
%   questions, bugs mail to d.b.t.broerse uu nl
%%

if exist('Settings')
   if Settings.ZRange(2)<=Settings.ZRange(1)
       error('make sure range is given as [zmin,zmax]')
   end
else
   error('Provide range of z values to be kept in Settings.ZRange')
end


%% extract points within z range

for i=1:length(ModelPoints)
    IndexKeep{i}=find(ModelPoints{i}.Z<=Settings.ZRange(2)&ModelPoints{i}.Z>=Settings.ZRange(1));
    IndexRemove{i}=~ismember([1:length(ModelPoints{i}.X)],IndexKeep{i});
    
    Outliers.Number(i)=length(find(IndexRemove{i}));
    Outliers.Percent(i)= Outliers.Number(i)/length(ModelPoints{i}.X)*100;
    % extract
    ModelPointsKeep{i}.time=ModelPoints{i}.time;
    ModelPointsKeep{i}.X=ModelPoints{i}.X(IndexKeep{i});
    ModelPointsKeep{i}.Y=ModelPoints{i}.Y(IndexKeep{i});
    ModelPointsKeep{i}.Z=ModelPoints{i}.Z(IndexKeep{i});
%     if i==1
%         figure; hold on
%         scatter3(ModelPointsKeep{i}.X,ModelPointsKeep{i}.Y,ModelPointsKeep{i}.Z,5,ModelPointsKeep{i}.Z);view([0 0 1]);colorbar
%         scatter3(ModelPoints{i}.X(IndexRemove{i}),ModelPoints{i}.Y(IndexRemove{i}),ModelPoints{i}.Z(IndexRemove{i}),20,'x')
%         axis('equal')
%         title('outliers')
%     end
    %hop
    % overwrite
    ModelPoints{i}=ModelPointsKeep{i};
end
end