function [ModelPoints,IndexModel,SizeModel]=SelectModel(Points,Settings,Op,Corrections)
%SelectModel lets user select points that contain the area of interest
%   [ModelPoints,IndexModel]=SelectModel(Points)
%
%   [Points] : structure containing ected X,Y and Z coordinates in
%   ected coordinates.
%
%   [ModelPoints] : structure containing ected X,Y and Z coordinates in
%   ected coordinates of the model area only.
%
%   [IndexModel] : structure containing ModelPoints index of Points
%
%   [SizeModel] : structure containing size of selected area
%
%   [ModelPoints,IndexModel]=SelectModel(Points,Settings)
%   [Settings] : structure containing settings. Should at least contain:
%       Settings.iStart : index of file that represents first scan, if not
%       given index is set to 1
%   has to contain as well: Settings.SelectionMethod, which can either be
%   'draw' or 'setrange'
%   in case 'setrange' is chosen, supply Settings.SelectAreaX = [xmin xmax]
%   and Settings.SelectAreaY = [ymin ymax]
%
%   [ModelPoints,IndexModel]=SelectModel(Points,Settings,Op,Corrections)
%   [Op] : structure that should contain Op.RemoveRefZ, whether or not a
%   reference Z should be subtracted
%   [Corrections] : if reference Z has to be subracted, supply
%   Corrections.Z0

%   DBT Broerse 2016-3-14
%   current version 2016-11-17
%   questions, bugs mail to d.b.t.broerse uu nl
%%

if exist('Settings','var')
    % if no index is given, take first
    j = 1;
else
    j = Settings.iStart;
end

% check if reference Z should be subtracted
if exist('Op','var')
    RemoveZ0=Op.RemoveRefZ;
    if exist('Corrections','var')
        RefZ0=Corrections.Z0;
    else
        error('provide Corrections.Z0 with a reference Z')
    end
else
    RemoveZ0=0;
end

if strcmp(Settings.SelectionMethod,'draw')
    
    fig=figure; hold on
    scatter3(Points{j}.X,Points{j}.Y,Points{j}.Z,5,Points{j}.Z);view([0 0 1]);colorbar
    %axis('image')
    title('draw rectangle containing area of interest')
    
    rect = getrect(gcf);
    close(fig)
    
    % rect contains [xmin ymin width height]
    % for output
    SizeModel.xmin=rect(1);
    SizeModel.ymin=rect(2);
    SizeModel.xwidth=rect(3);
    SizeModel.ywidth=rect(4);
    SizeModel.xmax=rect(1)+rect(3);
    SizeModel.ymax=rect(2)+rect(4);
    
elseif strcmp(Settings.SelectionMethod,'setrange')
    % get size of model from Settings
    SizeModel.xmin=Settings.SelectAreaX(1);
    SizeModel.ymin=Settings.SelectAreaY(1);
    SizeModel.xmax=Settings.SelectAreaX(2);
    SizeModel.ymax=Settings.SelectAreaY(2);
    SizeModel.xwidth=Settings.SelectAreaX(2)-Settings.SelectAreaX(1);
    SizeModel.ywidth=Settings.SelectAreaY(2)-Settings.SelectAreaY(1);
else
    error('invalid setting for Settings.SelectionMethod, choose either: draw or setrange')
end
%% extract points withing rect

for i=1:length(Points)
    IndexModel{i}=find(Points{i}.X<=SizeModel.xmax&Points{i}.X>=SizeModel.xmin&Points{i}.Y<=SizeModel.ymax&Points{i}.Y>=SizeModel.ymin);
    ModelPoints{i}.time=Points{i}.time;
    ModelPoints{i}.X=Points{i}.X(IndexModel{i});
    ModelPoints{i}.Y=Points{i}.Y(IndexModel{i});

    % remove reference height
        if RemoveZ0
            ModelPoints{i}.Z=Points{i}.Z(IndexModel{i})-RefZ0;
        else
            ModelPoints{i}.Z=Points{i}.Z(IndexModel{i});
        end
    if i==1
        figure; hold on
        scatter3(ModelPoints{i}.X,ModelPoints{i}.Y,ModelPoints{i}.Z,5,ModelPoints{i}.Z);view([0 0 1]);colorbar
        axis('image')
        title('selected model area')
    end
end
end