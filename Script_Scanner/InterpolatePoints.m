function [PointsArray]=InterpolatePoints(ModelPoints,Settings)
%InterpolatePoints interpolates scattered points to array
%   [PointsArray]=InterpolatePoints(ModelPoints,Settings)
%
%   [ModelPoints] : structure containing ected X,Y and Z coordinates in
%    vector
%
%   [Settings] : structure containing settings. Should at least contain:
%       Settings.InterpolateD : grid distance
%
%
%   [PointsArray] :[m x n] array with X, Y and Z coordinates

%   DBT Broerse 2016-11-16
%   current version
%   questions, bugs mail to d.b.t.broerse uu nl
%%

if exist('Settings')
    if Settings.InterpolateD  < 0
        error('provide grid distance larger than zero')
    end
else
    error('Provide grid distance in Settings.InterpolateD')
end

% if ~exist('Settings.InterpolationMethod')
%    Settings.InterpolationMethod = 'linear';
% end
%% extract points within z range
% grid distance
D=Settings.InterpolateD;

disp(strcat('interpolate at grid distance:',num2str(D)))
disp(' ' )
% loop over all laser scans
for i=1:length(ModelPoints)
    
    % determine grid range by current range
    Xmin=min(ceil(ModelPoints{i}.X));
    Xmax=max(floor(ModelPoints{i}.X));
    
    Ymin=min(ceil(ModelPoints{i}.Y));
    Ymax=max(floor(ModelPoints{i}.Y));
    
    
    % make vector
    xqvect=[Xmin:Settings.InterpolateD:Xmax];
    yqvect=[Ymin:Settings.InterpolateD:Ymax];
    % make array using meshgrid
    [xq,yq] = meshgrid(xqvect,yqvect);

    % interpolate (default is linear)
    PointsArray{i}.Z = griddata(ModelPoints{i}.X,ModelPoints{i}.Y,ModelPoints{i}.Z,xq,yq,Settings.InterpolationMethod);
    
    % save x and y to PointsArray
    PointsArray{i}.xvec = xqvect;
    PointsArray{i}.yvec = yqvect;    
end
disp('interpolation finished')
end