function [Points,Data] = ReadAllAscii(Data,Settings)
%readallascii reads ascii laser scanner data
%   [Points] = readallascii(Data)
%
%   [Points] : structure containing X,Y and Z coordinates in laser scanner
%   coordinates.
%
%   [Data] : structure containing information on data location and file
%   names. Should contain
%       Data.Dir : directory containing files
%       Data.Name : file name start
%       Data.Ext : extension of files
%
%  [Settings] : structure containing settings. Should at least contain:
%       Settings.iStart : index of file that represents first scan
%

%   DBT Broerse 2016-3-14
%   current version 2016-11-16
%   questions, bugs mail to d.b.t.broerse uu nl

% get all file names in given directory that start with Data.Name and have
% required extension
Data.Files = dir(strcat(Data.Dir,'/',Data.Name,'*',Data.Extension));
Data.NFiles = length(Data.Files);

% read data
for i=1:Data.NFiles
    
    Temp=importdata(strcat(Data.Dir,'/',Data.Files(i).name));
    % check if there is a Temp.data (which is the case when the file
    % contains a header)
    if isstruct(Temp)
        Points{i}.X=Temp.data(:,1);
        Points{i}.Y=Temp.data(:,2);
        Points{i}.Z=Temp.data(:,3);
        Points{i}.corrected=0;
    else
        % no header, so Temp contains only data and is not a structure
        Points{i}.X=Temp(:,1);
        Points{i}.Y=Temp(:,2);
        Points{i}.Z=Temp(:,3);
        Points{i}.corrected=0;
    end
    clear Temp
end

% determine time since start
StartTime = Data.Files(Settings.iStart).datenum;
Data.StartTime = Data.Files(Settings.iStart).date;
Points{Settings.iStart}.time=0;
for i=1:Data.NFiles
    
   Points{i}.time=Data.Files(i).datenum-StartTime;
end

end