% this script reads laser scanner data, corrects for misalignment (to get
% true heigth) and interpolates data
% requires Curve Fitting Toolbox
%
%   DBT Broerse 2016-3-14
%   current version 2016-11-16
%   questions, bugs mail to d.b.t.broerse uu nl

clear all
close all
clc
path(path,'C:\Script_Scanner');

% data locations and file name, adjust to needs
Data.Dir = 'D:\8- EXPERIMENT\EXPAA18\Scans\EXPAA18-SEL'; % location of laser files
Data.Name = 'Cloud_' % name of the laser files
Data.Extension = 'xyz' % laser file extension
Data.savedir = 'C:\Script_Scanner\EXPAA\EXPAA18-corrected'; % location to save results
Data.savename = 'Cloud_new' % name of results to save

% 1 set which file should be taken as reference
Settings.iStart=1; % index of t0

% 2 interpolation options %
Op.Interpolate = 1; % whether or not to interpolate to grid
Settings.InterpolateD = 0.1 % grid size for interpolation, units uncertain currently
Settings.InterpolationMethod = 'linear' % interpolation method, see griddata function

% 3 filter options %
Op.FilterZ =1; % whether or not remove points that lie outside Settings.ZRange
Settings.ZRange=[-2,18];%mm if Op.FilterZ == 1, all points outside this z range will be removed

Op.RemoveRefZ = 1;% remove offset in z direction (as computed from reference plane)

Op.PlotScatter = 0; % whether or not to plot original point cloud, can be memory consuming and slow in case of large files

% 4 selection of area of interest options and settings
Settings.SelectionMethod = 'draw' ; % options include 'setrange' or 'draw'
Settings.SelectAreaX = [-130 270];
Settings.SelectAreaY = [-210 160];

% set defaults
Outliers=[];
ModelPoints=[];
Points=[];
IndexPlane=[];
Corrections=[];
PointsArray=[];
%% read all laser scanner data
[Points,Data] = ReadAllAscii(Data,Settings);
%% get rectangle to select flat plane
[Plane,IndexPlane] = SelectFlatSurface(Points{Settings.iStart});
%% fit plane through selected points and correct all points (x,y,z coords) for rotations (around x and y axis) due to misalignment of laser with respect to z-axis
Settings.FitMethod='LSQplane'
[Points,Corrections] = RemoveMisalignment(Points,Plane,Settings);
%% select points that are within model boundaries
[ModelPoints,IndexModel,SizeModel]=SelectModel(Points,Settings,Op,Corrections);
%% filter points that fall within z range
if Op.FilterZ
    [ModelPoints,Outliers]=FilterZRange(ModelPoints,Settings);
end
%% interpolate data
if Op.Interpolate
    [PointsArray]=InterpolatePoints(ModelPoints,Settings);
end
%% plot interpolated data
if Op.Interpolate
    % determine maximum value for plot
    cmax=[];
    for i=1:length(PointsArray)
        cmax=max(abs([cmax ; PointsArray{end}.Z(:)]));cmin = -6;
    end
    for i=1:length(PointsArray)
        fig=figure; hold on
        % plot Z
        imagesc(PointsArray{i}.xvec,PointsArray{i}.yvec,PointsArray{i}.Z) ; colorbar
        % surf(PointsArray{1}.xvec, PointsArray{1}.yvec, PointsArray{1}.Z)
        % colormap summer
        % shading interp
        title(strcat('interpolated topography [mm] for i:',num2str(i),' grid size:',num2str(Settings.InterpolateD)))
        % fix color scale
        caxis([-2 8])
        axis('image')
        % save plot
        figfile=strcat(Data.savedir,'/',Data.savename,'_gridded_',num2str(Settings.InterpolateD),'index_',num2str(i));
        savefig(strcat(figfile,'.fig'))
        print(fig,strcat(figfile,'.png'),'-dpng')
    end
end
%% plot original data (may result in large files)
% determine maximum value
if Op.PlotScatter
    cmax=[];
    for i=1:length(ModelPoints)
        cmax=max(abs([cmax ; ModelPoints{end}.Z]));cmin=-2; % cmin = - cmax
        
    end
   
    for i=1:length(ModelPoints)
        fig=figure; hold on 
        scatter3(ModelPoints{i}.X,ModelPoints{i}.Y,ModelPoints{i}.Z,5,ModelPoints{i}.Z);view([0 0 1]);colorbar
        title(strcat('topography at time',num2str(ModelPoints{i}.time)))
        title(strcat('topography [mm] at index',num2str(i)))
        caxis([cmin cmax])
        xlim([SizeModel.xmin SizeModel.xmin+SizeModel.xwidth])
        ylim([SizeModel.ymin SizeModel.ymin+SizeModel.ywidth])
        axis('equal')
        shading interp
        figfile=strcat(Data.savedir,'/',Data.savename,num2str(i));
    
        savefig(strcat(figfile,'.fig'))
        print(fig,strcat(figfile,'.png'),'-dpng')
        %  close(fig)
    end
end
%% save data

savefile = strcat(Data.savedir,'/','results_saved.mat')
save(savefile,'PointsArray','ModelPoints','Settings','Op','Corrections')




%% save interpolated data as ascii
% for writing xyz files use fprintf
for i=1:length(PointsArray)
    % make arrays of x and y (until now we only have vectors)
    [PointsArray{i}.X,PointsArray{i}.Y] = meshgrid(PointsArray{i}.xvec,PointsArray{i}.yvec);
    [~,FileName,~]=fileparts(Data.Files(i).name);
    savefile=strcat(Data.savedir,'/','Interpolatedfield_gridsize_9',num2str(Settings.InterpolateD),'_',FileName,'.txt')
    writearray=[PointsArray{i}.X(:)';PointsArray{i}.Y(:)';PointsArray{i}.Z(:)'];
    fid=fopen(savefile,'w');
    % write to file
    fprintf(fid,'%12.4e %12.4e %12.4e\n',writearray);
    fclose(fid)
    
end




% for writing ascii files use fprintf
%
% example:
% x = 0:.1:1;
%         y = [x; exp(x)];
%         fid = fopen('exp.txt','w');
%         fprintf(fid,'%6.2f  %12.8f\n',y);
%         fclose(fid);

%% make movie
% not sure whether this functions well
%
% figure
% scatter3(ModelPoints{i}.X,ModelPoints{i}.Y,ModelPoints{i}.Z,5,ModelPoints{i}.Z)
% view([0 0 1])
% colorbar
% caxis([cmin cmax])
% axis equal
% ax = gca;
% ax.NextPlot = 'replaceChildren';

% video = VideoWriter(strcat(Data.Dir,'/',Data.Name,'movie.jpeg'),'Archival');
% open(video)
%
%
% loops = length(ModelPoints);
% F(loops) = struct('cdata',[],'colormap',[]);
% for i = 10:20%loops
%
%     scatter3(ModelPoints{i}.X,ModelPoints{i}.Y,ModelPoints{i}.Z,5,ModelPoints{i}.Z)
%     view([0 0 1])
%     colorbar
%     caxis([cmin cmax])
%     xlim([SizeModel.xmin SizeModel.xmin+SizeModel.xwidth])
%     ylim([SizeModel.ymin SizeModel.ymin+SizeModel.ywidth])
%     drawnow
%     F(i) = getframe;
%     axis equal
%     writeVideo(video,F(i))
% end
%
%
% close(video)

