function [Points,Corrections] = RemoveMisalignment(PointsUncorrected,Plane,Settings)
%removemisalignment fits a plane to a user selected rectangle of points and
%uses angles of this plane to correct all data for laser misalignment with
%vertical.
%
%   [Points,RotAngle] = RemoveMisalignment(Points,Plane,Settings)
%
%   [Points] : structure containing corrected X,Y and Z coordinates in
%   corrected coordinates.
%
%   [Corrections] : structure containing applied corrections to data:
%   rotation around x and y axis, determined using [Plane]
%
%   [Settings] : structure containing settings. Should at least contain:
%       Settings.FitMethod = 'poly11' (using fit) or 'LSQplane' (no need
%       for curve fitting toolbox)
%
%
%   [PointsUncorrected] : structure containing corrected X,Y and Z coordinates in laser scanner
%   coordinates.
%
%   [Plane] : structure containing X,Y and Z coordinates of plane in laser
%   scanner coordinates.
%
%

%   DBT Broerse 2016-3-14
%   current version 2016-11-16
%   questions, bugs mail to d.b.t.broerse uu nl

% this makes use of curve fitting toolbox
%% use fit function from matlab and fit a straight plane
if strcmp(Settings.FitMethod,'poly11')
    [Plane.fo,Plane.g] = fit( [Plane.X, Plane.Y], Plane.Z, 'poly11' );
    % Plane.fo.p10 = dz/dx
    % Plane.fo.p00 = z at (0,0)
    % Plane.fo.p01 = dz/dy
    figure
    plot(Plane.fo,[Plane.X,Plane.Y],Plane.Z);colorbar;view([0 1 0]);
    title('fit to plane')
elseif strcmp(Settings.FitMethod,'LSQplane')
    % this option does not need curve fitting toolbox
    X = [ones(size(Plane.X)) Plane.X Plane.Y];
    a = X\Plane.Z;
    Plane.fo.p00=a(1);
    Plane.fo.p10=a(2);
    Plane.fo.p01=a(3);
end





%% rotation angles
Corrections.RotAngleY=atan2d(Plane.fo.p10,1);
Corrections.RotAngleX=-atan2d(Plane.fo.p01,1);



%% apply rotations around x and y axes
Plane.Corrected=rotatex(Corrections.RotAngleX)*rotatey(Corrections.RotAngleY)*[Plane.X  Plane.Y  Plane.Z]';
Plane.Corrected=Plane.Corrected';

figure;subplot(1,2,1)
scatter3(Plane.X,Plane.Y,Plane.Z,2,Plane.Z);colorbar;view([0 0 1])
title('original')
axis image
subplot(1,2,2)
scatter3(Plane.Corrected(:,1),Plane.Corrected(:,2),Plane.Corrected(:,3),2,Plane.Corrected(:,3));colorbar;view([0 0 1])
title('corrected')
axis image

Corrections.Z0 = mean(Plane.Corrected(:,3));
%% correct all data

for i=1:length(PointsUncorrected)
    if (PointsUncorrected{i}.corrected==0)
        Points{i}.time=PointsUncorrected{i}.time;
        CorrectedPoints=rotatex(Corrections.RotAngleX)*rotatey(Corrections.RotAngleY)*[PointsUncorrected{i}.X  PointsUncorrected{i}.Y  PointsUncorrected{i}.Z]';
        Points{i}.X=CorrectedPoints(1,:)';
        Points{i}.Y=CorrectedPoints(2,:)';
        Points{i}.Z=CorrectedPoints(3,:)';
        Points{i}.corrected=1;
    else
        error('points have already been corrected')
    end
end

disp('correction of tilt in images finished')

end

% rotation matrices
function Rx = rotatex(angle)

Rx = [1 0 0; 0 cosd(angle) -sind(angle); 0 sind(angle) cosd(angle)];

end

function Ry = rotatey(angle)

Ry = [cosd(angle) 0 sind(angle); 0 1 0; -sind(angle) 0 cosd(angle)];

end
